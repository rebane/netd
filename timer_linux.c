#include "timer_linux.h"
#include <stdint.h>
#include <time.h>
#include <unistd.h>

void timer_linux_init(){
}

uint32_t timer_linux_get(uint16_t *msec){
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	*msec = ts.tv_nsec / 1000000LLU;
	return(ts.tv_sec);
}

