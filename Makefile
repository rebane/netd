CROSS_COMPILE ?= arm-linux-gnueabi-
# CROSS_COMPILE ?= /opt/mipsel-openwrt-linux/bin/mipsel-openwrt-linux-

all:
	$(CROSS_COMPILE)gcc -Wall -Wno-format-truncation main.c timer_linux.c mtimer.c client.c json.c socket_ud.c daemon.c dhcpc.c process.c callback.c netconf.c interface.c interface_lan.c interface_eg25.c -lpthread -o netd
	$(CROSS_COMPILE)strip netd

arm:
	arm-linux-gnueabi-gcc -Wall -Wno-format-truncation main.c timer_linux.c mtimer.c client.c json.c socket_ud.c daemon.c dhcpc.c process.c callback.c netconf.c interface.c interface_lan.c interface_eg25.c -lpthread -static -o netd
	arm-linux-gnueabi-strip netd

