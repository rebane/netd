#include "socket_ud.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

int socket_ud(){
	int sock;

	sock = socket(PF_UNIX, SOCK_DGRAM, 0);
	if(sock < 0)return(sock);
	return(sock);
}

int socket_ud_bind(char *filename){
	struct sockaddr_un addr;
	int sock, t;

	sock = socket(PF_UNIX, SOCK_DGRAM, 0);
	if(sock < 0)return(sock);

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, filename, sizeof(addr.sun_path));
	unlink(addr.sun_path);
	t = bind(sock, (struct sockaddr *)&addr, sizeof(addr));
	if(t < 0){
		socket_ud_close(sock);
		return(t);
	}
	return(sock);
}

ssize_t socket_ud_recvfrom(int socket, int flags, void *buf, size_t len, char *filename){
	struct sockaddr_un addr;
	ssize_t l;
	int t;

	memset(&addr, 0, sizeof(addr));
	t = sizeof(addr);
	l = recvfrom(socket, buf, len, flags, (struct sockaddr *)&addr, (socklen_t *)&t);
	if(filename != NULL)strcpy(filename, addr.sun_path);
	if(l < 0)return(l);
	return(l);
}

ssize_t socket_ud_sendto(int socket, int flags, const void *buf, size_t len, const char *filename){
	struct sockaddr_un addr;
	ssize_t l;

	if(filename == NULL){
		l = send(socket, buf, len, flags);
	}else{
		memset(&addr, 0, sizeof(addr));
		addr.sun_family = AF_UNIX;
		strncpy(addr.sun_path, filename, sizeof(addr.sun_path));

		l = sendto(socket, buf, len, flags, (struct sockaddr *)&addr, sizeof(addr));
	}
	if(l < 0)return(l);
	return(l);
}

void socket_ud_close(int socket){
	shutdown(socket, SHUT_RDWR);
	close(socket);
}

