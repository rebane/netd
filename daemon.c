#include "daemon.h"
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

enum {
	DAEMON_STATUS_OFF,
	DAEMON_STATUS_STARTING,
	DAEMON_STATUS_ON,
	DAEMON_STATUS_STOPPING
};

static void *daemon_thread(void *p);

struct daemon_t *daemon_new(daemon_proc_t proc, void *user)
{
	struct daemon_t *d;

	d = malloc(sizeof(*d));

	if (d == NULL)
		return NULL;

	if (pthread_mutex_init(&d->lock, NULL)) {
		free(d);
		return NULL;
	}

	d->status = DAEMON_STATUS_OFF;
	d->proc = proc;
	d->user = user;
	return d;
}

void daemon_free(struct daemon_t *d)
{
	free(d);
}

void daemon_start(struct daemon_t *d)
{
	int start = 0;
	pthread_mutex_lock(&d->lock);
	if (d->status == DAEMON_STATUS_OFF) {
		d->status = DAEMON_STATUS_STARTING;
		start = 1;
	}
	pthread_mutex_unlock(&d->lock);
	if (!start)
		return;

	pthread_create(&d->thread, NULL, daemon_thread, d);
	pthread_mutex_lock(&d->lock);
	d->status = DAEMON_STATUS_ON;
	pthread_mutex_unlock(&d->lock);
}

void daemon_stop(struct daemon_t *d)
{
	int join = 0;
	pthread_mutex_lock(&d->lock);
	if (d->status == DAEMON_STATUS_ON) {
		d->status = DAEMON_STATUS_STOPPING;
		join = 1;
	}
	pthread_mutex_unlock(&d->lock);
	if (!join)
		return;

	pthread_join(d->thread, NULL);
	pthread_mutex_lock(&d->lock);
	d->status = DAEMON_STATUS_OFF;
	pthread_mutex_unlock(&d->lock);
}

void daemon_restart(struct daemon_t *d)
{
	daemon_stop(d);
	daemon_start(d);
}

int daemon_test(struct daemon_t *d, unsigned int timeout)
{
	unsigned int i;
	int killed = 0;

	for (i = 0; i < timeout; i += 100) {
		pthread_mutex_lock(&d->lock);
		if ((d->status == DAEMON_STATUS_STOPPING) ||
				(d->status == DAEMON_STATUS_OFF))
			killed = 1;
		pthread_mutex_unlock(&d->lock);
		if (killed)
			return 1;
		if (timeout)
			usleep(100000);
	}
	return 0;
}

void daemon_exit(struct daemon_t *d)
{
	pthread_exit(NULL);
}

static void *daemon_thread(void *p)
{
	struct daemon_t *d = p;
	daemon_proc_t proc = d->proc;

	proc(d, d->user);
	return NULL;
}

