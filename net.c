#include "net.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "json.h"

static pthread_mutex_t net_lock;

void net_init(){
	pthread_mutex_init(&net_lock, NULL);
}

int net_json2inet(char *dest, json_object_t *value){
	dest[0] = 0;
	if(value->type != JSON_TYPE_STRING)return(0);
	if(value->len < 3)return(0);
	return(net_str2inet(dest, &value->start[1], value->len - 2));
}

int net_str2inet(char *dest, void *ptr, int len){
	char buffer[16];
	struct in_addr addr;
	int i;
	dest[0] = 0;
	if(len > 15)return(0);
	memcpy(buffer, ptr, len);
	buffer[len] = 0;
	pthread_mutex_lock(&net_lock);
	i = inet_aton(buffer, &addr);
	if(i)strncpy(dest, inet_ntoa(addr), 16);
	pthread_mutex_unlock(&net_lock);
	if(!i)return(0);
	dest[15] = 0;
	return(1);
}

void net_up(char *interface, int type, char *ip, char *netmask, char *broadcast, char *pointopoint, char *gateway, char *dns1, char *dns2){
}

void net_down(char *interface){
}

void net_ifconfig(char *interface, char *ip, char *netmask, char *broadcast, int up){
	int l;
	char command[256];
	l = 0;
	if(up){
		if((ip != NULL) && ip[0]){
			l += snprintf(&command[l], 256 - l, "/bin/busybox ifconfig %s %s", interface, ip);
			if((netmask != NULL) && netmask[0])l += snprintf(&command[l], 256 - l, " netmask %s", netmask);
			if((broadcast != NULL) && broadcast[0])l += snprintf(&command[l], 256 - l, " broadcast %s", broadcast);
			l += snprintf(&command[l], 256 - l, " up");
		}else{
			l += snprintf(&command[l], 256 - l, "/bin/busybox ifconfig %s up", interface);
		}
	}else{
		l += snprintf(&command[l], 256 - l, "/bin/busybox ifconfig %s down", interface);
	}
	system(command);
}

void net_route_default(char *interface, char *gateway){
	int l;
	char command[256];
	l = 0;
	if((gateway == NULL) || !gateway[0])return;
	system("/bin/busybox route del default >/dev/null 2>&1");
	system("/bin/busybox route del default >/dev/null 2>&1");
	l = 0;
	l += snprintf(&command[l], 256 - l, "/bin/busybox route add default gw %s dev %s", gateway, interface);
	system(command);
}

void net_dns(char *interface, char *dns1, char *dns2){
}

