#include "interface.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <net/if.h>
#include <linux/ethtool.h>
#include <linux/sockios.h>
#include "json.h"
#include "daemon.h"
#include "dhcpc.h"

struct interface_t *interface_new(json_object_t *interface, json_object_t *configuration)
{
	struct interface_t *in;
	json_object_t interface_type;
	json_object_t configuration_mode;

	if (!json_get_value(interface->start, interface->len, "type", &interface_type))
		return NULL;

	in = malloc(sizeof(*in));
	if (in == NULL)
		return NULL;

	in->daemon = NULL;
	in->interface = *interface;
	in->configuration = *configuration;

	if (json_isequal(&interface_type, "lan")) {
		if (json_get_value(configuration->start, configuration->len, "mode", &configuration_mode)) {
			if (json_isequal(&configuration_mode, "dynamic")) {
				in->daemon = daemon_new(interface_lan_dynamic_proc, in);
			} else if (json_isequal(&configuration_mode, "static")) {
				in->daemon = daemon_new(interface_lan_static_proc, in);
			}
		}
	} else if (json_isequal(&interface_type, "eg25g")) {
		in->daemon = daemon_new(interface_eg25_proc, in);
	}

	if (in->daemon == NULL) {
		free(in);
		return NULL;
	}

	return in;
}

void interface_start(struct interface_t *in)
{
	daemon_start(in->daemon);
}

void interface_stop(struct interface_t *in)
{
	daemon_stop(in->daemon);
}

int interface_link_get(const char *iface){
	int fd, retval;
	struct ifreq ifr;
	struct ethtool_value e;

	fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0)
		return -1;

	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);
	e.cmd = ETHTOOL_GLINK;
	ifr.ifr_data = (char *)&e;
	retval = ioctl(fd, SIOCETHTOOL, &ifr);
	if (!retval)
		retval = e.data ? 1 : 0;

	shutdown(fd, SHUT_RDWR);
	close(fd);
	return retval;
}

