#ifndef _SOCKET_UD_H_
#define _SOCKET_UD_H_

#include <stdio.h>

int socket_ud();
int socket_ud_bind(char *filename);
ssize_t socket_ud_recvfrom(int socket, int flags, void *buf, size_t len, char *filename);
ssize_t socket_ud_sendto(int socket, int flags, const void *buf, size_t len, const char *filename);
void socket_ud_close(int socket);

#endif

