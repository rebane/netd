#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include "json.h"
#include "socket_ud.h"

#define JSON_SIZE 65536

extern char **environ;

static char *client_env_parse(char *env, char *env_key);

int client(int argc, char *argv[]){
	char json[JSON_SIZE], env_key[65536], bind_buf[256], *env_val;
	int i, l, s;

	l = 0;
	l += snprintf(&json[l], JSON_SIZE - l, "{\"command\":\"callback\",\"data\":{");

	// name
	l += snprintf(&json[l], JSON_SIZE - l, "\"command\":\"");
	l += strtojson(&json[l], argv[0], JSON_SIZE - l);
	l += snprintf(&json[l], JSON_SIZE - l, "\"");

	l += snprintf(&json[l], JSON_SIZE - l, ",");

	// args
	l += snprintf(&json[l], JSON_SIZE - l, "\"arguments\":[");
	for(i = 1; i < argc; i++){
		if(i == 1){
			l += snprintf(&json[l], JSON_SIZE - l, "\"");
		}else{
			l += snprintf(&json[l], JSON_SIZE - l, ",\"");
		}
		l += strtojson(&json[l], argv[i], JSON_SIZE - l);
		l += snprintf(&json[l], JSON_SIZE - l, "\"");
	}
	l += snprintf(&json[l], JSON_SIZE - l, "]");

	l += snprintf(&json[l], JSON_SIZE - l, ",");

	// pid
	l += snprintf(&json[l], JSON_SIZE - l, "\"pid\":%ld", (long int)getpid());

	l += snprintf(&json[l], JSON_SIZE - l, ",");

	// ppid
	l += snprintf(&json[l], JSON_SIZE - l, "\"ppid\":%ld", (long int)getppid());

	l += snprintf(&json[l], JSON_SIZE - l, ",");

	// environ
	l += snprintf(&json[l], JSON_SIZE - l, "\"environ\":{");
	for(i = 0; environ[i]; i++){
		env_val = client_env_parse(environ[i], env_key);
		if(env_val == NULL)continue;
		if(i == 0){
			l += snprintf(&json[l], JSON_SIZE - l, "\"");
		}else{
			l += snprintf(&json[l], JSON_SIZE - l, ",\"");
		}
		l += strtojson(&json[l], env_key, JSON_SIZE - l);
		l += snprintf(&json[l], JSON_SIZE - l, "\":\"");
		l += strtojson(&json[l], env_val, JSON_SIZE - l);
		l += snprintf(&json[l], JSON_SIZE - l, "\"");
	}
	l += snprintf(&json[l], JSON_SIZE - l, "}");

	l += snprintf(&json[l], JSON_SIZE - l, "}}");

	json[l] = 0;

//	printf("%s\n", json);
	snprintf(bind_buf, 256, "/tmp/netd-%d", (int)getpid());
	s = socket_ud_bind(bind_buf);
	if(s < 0){
		unlink(bind_buf);
		fprintf(stderr, "socket\n");
		return(1);
	}
	if(socket_ud_sendto(s, 0, json, l, "/tmp/netd-socket") < 0){
		fprintf(stderr, "send\n");
		socket_ud_close(s);
		unlink(bind_buf);
		return(1);
	}

	socket_ud_recvfrom(s, 0, json, 65536, NULL);
	socket_ud_close(s);
	unlink(bind_buf);
	return(0);
}

static char *client_env_parse(char *env, char *env_key){
	int i;
	for(i = 0; (i < 65536) && env[i]; i++){
		if(env[i] == '='){
			env_key[i] = 0;
			return(&env[i + 1]);
		}
		env_key[i] = env[i];
	}
	return(NULL);
}

