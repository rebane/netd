#ifndef _CALLBACK_H_
#define _CALLBACK_H_

typedef int (*callback_proc_t)(void *buffer, int len, void *user);

void callback_init();
void callback(void *buffer, int len);
void callback_register(callback_proc_t proc, void *user);
void callback_unregister(callback_proc_t proc, void *user);

#endif

