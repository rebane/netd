#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "timer_linux.h"
#include "mtimer.h"
#include "client.h"
#include "json.h"
#include "socket_ud.h"
#include "callback.h"
#include "interface.h"

char *json_strdup(json_object_t *object)
{
	char *str;

	str = malloc(object->len);
	memcpy(str, &object->start[1], object->len - 2);
	str[object->len - 2] = 0;

	return str;
}

int main(int argc, char *argv[])
{
	char config[65536], buffer[65536], path[256];
	json_object_t interfaces, interface_name, interface, interface_type, configurations, configuration_name, configuration, data;
	int config_len;
	int fd, s, l, i, j;
	struct interface_t *in;

	if ((getenv("NETD_MAGIC") != NULL) && !strcmp(getenv("NETD_MAGIC"), "143AIO5G6"))
		return client(argc, argv);

	timer_linux_init();
	mtimer_init(timer_linux_get);

	setenv("NETD_MAGIC", "143AIO5G6", 1);

	if (argc < 2) {
		printf("missing config file\n");
		return(1);
	}
	fd = open(argv[1], O_RDONLY);
	if (fd < 0){
		printf("cannot open config file\n");
		return(1);
	}
	config_len = read(fd, config, 65536);
	close(fd);

	netconf_init();
	callback_init();

	s = socket_ud_bind("/tmp/netd-socket");
	if(s < 0) {
		fprintf(stderr, "socket\n");
		exit(1);
	}

	if (json_get_value((unsigned char *)config, config_len, "interfaces", &interfaces)) {
		for(i = 0; json_get(interfaces.start, interfaces.len, i, &interface_name, &interface); i++){
			if (!json_get_value(interface.start, interface.len, "type", &interface_type))continue;
			if (!json_get_value(interface.start, interface.len, "configurations", &configurations))continue;
			for(j = 0; json_get(configurations.start, configurations.len, j, &configuration_name, &configuration); j++){
				in = interface_new(&interface, &configuration);
				if (in != NULL) {
					printf("INTERFACE: %s, TYPE: %s, CONF: %s\n", json_strdup(&interface_name), json_strdup(&interface_type), json_strdup(&configuration_name));
					interface_start(in);
				}
			}
		}
	}

	while (1) {
		l = socket_ud_recvfrom(s, 0, buffer, 65536, path);
		buffer[l] = 0;
		if(json_value_isequal((unsigned char *)buffer, l, "command", "callback")) {
			if(json_get_value((unsigned char *)buffer, l, "data", &data)) {
				callback(data.start, data.len);
				socket_ud_sendto(s, 0, "{\"command\":\"ack\"}", 17, path);
			}
		}
	}
}

