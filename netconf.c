#include "netconf.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "json.h"

#define NETCONF_GW_LIST_SIZE 8

static struct {
	int up;
	char interface[16];
	char gw[16];
	char ns1[16];
	char ns2[16];
} netconf_gw_list[NETCONF_GW_LIST_SIZE];

int netconf_json2inet(char *dest, json_object_t *value)
{
	dest[0] = 0;
	if (value->type != JSON_TYPE_STRING)
		return 0;
	if (value->len < 3)
		return 0;

	return netconf_str2inet(dest, &value->start[1], value->len - 2);
}

int netconf_str2inet(char *dest, void *ptr, int len)
{
	char buffer[16];
	struct in_addr addr;
	int i;

	dest[0] = 0;

	if (len > 15)
		return 0;

	memcpy(buffer, ptr, len);
	buffer[len] = 0;
	i = inet_aton(buffer, &addr);

	if (i)
		strncpy(dest, inet_ntoa(addr), 16);

	if (!i)
		return 0;

	dest[15] = 0;

	return 1;
}

void netconf_init()
{
	int i;

	for (i = 0; i < NETCONF_GW_LIST_SIZE; i++) {
		netconf_gw_list[i].up = 0;
		netconf_gw_list[i].gw[0] = 0;
		netconf_gw_list[i].ns1[0] = 0;
		netconf_gw_list[i].ns2[0] = 0;
	}
}

void netconf_ifconfig(char *interface, char *ip, char *netmask, char *broadcast, char *gateway, char *ns1, char *ns2, int up)
{
	char command[256];
	int priority;
	int i, l = 0;

	priority = 1;
	if (!strcmp(interface, "eth0"))
		priority = 0;

	printf("INTERFACE: %s, %d, %d\n", interface, up, priority);

	if (up) {
		if ((ip != NULL) && ip[0]) {
			l += snprintf(&command[l], 256 - l, "/bin/busybox ifconfig %s %s", interface, ip);
			if ((netmask != NULL) && netmask[0])
				l += snprintf(&command[l], 256 - l, " netmask %s", netmask);
			if ((broadcast != NULL) && broadcast[0])
				l += snprintf(&command[l], 256 - l, " broadcast %s", broadcast);
			l += snprintf(&command[l], 256 - l, " up");
		} else {
			l += snprintf(&command[l], 256 - l, "/bin/busybox ifconfig %s up", interface);
		}
		system(command);
	} else {
		l += snprintf(&command[l], 256 - l, "/bin/busybox ifconfig %s 0", interface);
		system(command);
	}

	netconf_gw_list[priority].up = up;
	strncpy(netconf_gw_list[priority].interface, interface, 16);
	if (gateway != NULL)
		strncpy(netconf_gw_list[priority].gw, gateway, 16);
	else
		netconf_gw_list[priority].gw[0] = 0;
	if (ns1 != NULL)
		strncpy(netconf_gw_list[priority].ns1, ns1, 16);
	else
		netconf_gw_list[priority].ns1[0] = 0;
	if (ns2 != NULL)
		strncpy(netconf_gw_list[priority].ns2, ns2, 16);
	else
		netconf_gw_list[priority].ns2[0] = 0;

	for (i = 0; i < NETCONF_GW_LIST_SIZE; i++) {
		if (netconf_gw_list[i].up && (netconf_gw_list[i].gw[0] != 0)) {
			netconf_route_default(netconf_gw_list[i].interface, netconf_gw_list[i].gw);
			
			snprintf(command, 256, "/etc/netd.sh dns %s %s %s", netconf_gw_list[i].interface, netconf_gw_list[i].ns1, netconf_gw_list[i].ns2);
			command[255] = 0;
			system(command);
			break;
		}
	}
}

void netconf_route_default(char *interface, char *gateway)
{
	int l = 0;
	char command[256];

	if ((gateway == NULL) || !gateway[0])
		return;

	printf("SETTING GATEWAY: %s\n", gateway);

	system("/bin/busybox route del default >/dev/null 2>&1");
	system("/bin/busybox route del default >/dev/null 2>&1");

	l += snprintf(&command[l], 256 - l, "/bin/busybox route add default gw %s dev %s", gateway, interface);
	system(command);
}

