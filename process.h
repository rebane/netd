#ifndef _PROCESS_H_
#define _PROCESS_H_

#include <stdarg.h>

int process(char *command, ...);
int process_done(int pid, int *ret);
void process_kill(int pid, unsigned int timeout);

#endif

