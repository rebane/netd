#include "callback.h"
#include <stdio.h>
#include <pthread.h>

#define CALLBACK_LIST_LEN 32

static struct {
	callback_proc_t proc;
	void *user;
}callback_list[CALLBACK_LIST_LEN];

static pthread_mutex_t callback_lock;

void callback_init()
{
	int i;

	for (i = 0; i < CALLBACK_LIST_LEN; i++)
		callback_list[i].proc = NULL;

	pthread_mutex_init(&callback_lock, NULL);
}

void callback(void *buffer, int len)
{
	int i;

	pthread_mutex_lock(&callback_lock);
	for (i = 0; i < CALLBACK_LIST_LEN; i++) {
		if (callback_list[i].proc != NULL) {
			if (callback_list[i].proc(buffer, len, callback_list[i].user) >= 0)
				break;
		}
	}
	pthread_mutex_unlock(&callback_lock);
}

void callback_register(callback_proc_t proc, void *user)
{
	int i;

	pthread_mutex_lock(&callback_lock);
	for (i = 0; i < CALLBACK_LIST_LEN; i++) {
		if ((callback_list[i].proc == proc) && (callback_list[i].user == user))
			break;
	}
	if (i >= CALLBACK_LIST_LEN) {
		for (i = 0; i < CALLBACK_LIST_LEN; i++) {
			if (callback_list[i].proc == NULL) {
				callback_list[i].proc = proc;
				callback_list[i].user = user;
				break;
			}
		}
	}
	pthread_mutex_unlock(&callback_lock);
}

void callback_unregister(callback_proc_t proc, void *user)
{
	int i;

	pthread_mutex_lock(&callback_lock);
	for (i = 0; i < CALLBACK_LIST_LEN; i++) {
		if ((callback_list[i].proc == proc) && (callback_list[i].user == user)) {
			callback_list[i].proc = NULL;
		}
	}
	pthread_mutex_unlock(&callback_lock);
}

