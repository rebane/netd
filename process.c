#include "process.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

int process(char *command, ...){
	pid_t pid;
	char *argv[256];
	va_list args;
	int i, ret;
	pid = fork();
	if(pid != 0)return(pid);
	signal(SIGCHLD, SIG_IGN);
	va_start(args, command);
	argv[0] = command;
	for(i = 1; i < 255; i++){
		argv[i] = va_arg(args, char *);
		if(argv[i] == NULL)break;
	}
	argv[255] = NULL;
	ret = execv(command, argv);
	va_end(args);
	exit(-1);
	return(ret);
}

int process_done(int pid, int *ret)
{
	int status;

	if (waitpid(pid, &status, WNOHANG) != pid)
		return 0;

	if (!WIFEXITED(status))
		return 0;

	if(ret != NULL)
		*ret = WEXITSTATUS(status);

	return 1;
}

void process_kill(int pid, unsigned int timeout)
{
	unsigned int i;

	kill(pid, SIGTERM);

	for (i = 0; i < timeout; i += 100) {
		if (process_done(pid, NULL))
			return;

		if (timeout)
			usleep(100000);
	}

	kill(pid, SIGKILL);
	for (i = 0; i < 2; i++) {
		if (process_done(pid, NULL))
			return;

		usleep(100000);
	}
}

