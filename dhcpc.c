#include "dhcpc.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "daemon.h"
#include "process.h"
#include "callback.h"

static void dhcpc_proc(struct daemon_t *d, void *p);
static int dhcpc_callback(void *buffer, int len, void *user);

struct dhcpc_t *dhcpc_new()
{
	struct dhcpc_t *dhcpc;

	dhcpc = malloc(sizeof(*dhcpc));
	if (dhcpc == NULL)
		return NULL;

	if (pthread_mutex_init(&dhcpc->lock, NULL)) {
		free(dhcpc);
		return NULL;
	}

	dhcpc->daemon = daemon_new(dhcpc_proc, dhcpc);
	if (dhcpc->daemon == NULL) {
		free(dhcpc);
		return NULL;
	}

	dhcpc->offer = 0;
	dhcpc->refresh = 0;

	return dhcpc;
}

void dhcpc_free(struct dhcpc_t *dhcpc)
{
	daemon_stop(dhcpc->daemon);
	daemon_free(dhcpc->daemon);
	free(dhcpc);
}

void dhcpc_start(struct dhcpc_t *dhcpc, char *interface)
{
	strncpy(dhcpc->interface, interface, 15);
	dhcpc->interface[15] = 0;
	daemon_start(dhcpc->daemon);
}

void dhcpc_stop(struct dhcpc_t *dhcpc)
{
	daemon_stop(dhcpc->daemon);
}

int dhcpc_netconf(struct dhcpc_t *dhcpc, struct netconf_t *nc)
{
	int ret = 0;

	pthread_mutex_lock(&dhcpc->lock);
	if (dhcpc->offer && dhcpc->refresh) {
		memcpy(nc, (void *)&dhcpc->nc, sizeof(struct netconf_t));
		dhcpc->refresh = 0;
		ret = 1;
	}
	pthread_mutex_unlock(&dhcpc->lock);
	return ret;
}

static void dhcpc_proc(struct daemon_t *d, void *user)
{
	struct dhcpc_t *dhcpc = user;
	char buffer[1024];
	int i;

	dhcpc->refresh = 0;

	while(1) {
		dhcpc->offer = 0;
		callback_register(dhcpc_callback, dhcpc);

		snprintf(buffer, 1020, "ifconfig %s up", dhcpc->interface);
		system(buffer);

		dhcpc->pid = process("/bin/busybox", "udhcpc", "-i", dhcpc->interface, "-s", "/bin/netdc", "-q", "-f", NULL);
		printf("PID: %d\n", dhcpc->pid);
		for (i = 0; i < 30; i++) {
			if (dhcpc->offer || process_done(dhcpc->pid, NULL))
				break;

			if (daemon_test(d, 1000)) {
				process_kill(dhcpc->pid, 3000);
				callback_unregister(dhcpc_callback, dhcpc);
				daemon_exit(d);
			}
		}

		process_kill(dhcpc->pid, 3000);
		callback_unregister(dhcpc_callback, dhcpc);

		if (!dhcpc->offer) {
			if (daemon_test(d, 5000))
				daemon_exit(d);
			continue;
		}

		if (daemon_test(d, dhcpc->lease))
			daemon_exit(d);
	}
}

static int dhcpc_callback(void *buffer, int len, void *user)
{
	struct dhcpc_t *dhcpc = user;
	json_object_t value, arguments, environ;
	int s, i, c;

	pthread_mutex_lock(&dhcpc->lock);
	pthread_mutex_unlock(&dhcpc->lock);

	// {"command":"/bin/netdc","arguments":["deconfig"],"pid":106,"ppid":105,"environ":{"SHLVL":"1","HOME":"/root","TERM":"linux","PWD":"/root","NETD_MAGIC":"143AIO5G6","interface":"eth0"}}

	// {"command":"/bin/netdc","arguments":["bound"],"pid":117,"ppid":115,"environ":{"SHLVL":"1","HOME":"/root","TERM":"linux","PWD":"/root","NETD_MAGIC":"143AIO5G6","interface":"eth0","ip":"192.168.1.156","siaddr":"192.168.1.1","subnet":"255.255.255.0","mask":"24","router":"192.168.1.1","dns":"192.98.49.8 192.98.49.9","broadcast":"192.168.1.255","lease":"3600","serverid":"192.168.1.1","opt53":"05","opt58":"00000708","opt59":"00000c4e"}}

	printf("DHCPC CALLBACK\n");
	fflush(stdout);
	write(1, buffer, len);
	printf("\n");

	if (!json_get_value(buffer, len, "ppid", &value))
		return -1;

	if (json_int(&value) != dhcpc->pid)
		return -1;

	if (!json_get_value(buffer, len, "arguments", &arguments))
		return -1;

	if (!json_get(arguments.start, arguments.len, 0, NULL, &value))
		return -1;

	if (!json_isequal(&value, "bound"))
		return 0;

	if (!json_get_value(buffer, len, "environ", &environ))
		return 0;

	if (!json_value_isequal(environ.start, environ.len, "interface", dhcpc->interface))
		return 0;

	if (!json_get_value(environ.start, environ.len, "ip", &value))
		return 0;

	if (!netconf_json2inet((void *)dhcpc->nc.ip, &value))
		return 0;

	dhcpc->nc.nm[0] = dhcpc->nc.bc[0] = dhcpc->nc.gw[0] = dhcpc->nc.ns1[0] = dhcpc->nc.ns2[0] = 0;

	if (json_get_value(environ.start, environ.len, "lease", &value)) {
		dhcpc->lease = 1000LU * json_int(&value);
	}else{
		dhcpc->lease = 1800000LU;
	}

	if (json_get_value(environ.start, environ.len, "subnet", &value)) {
		netconf_json2inet((void *)dhcpc->nc.nm, &value);
	}

	if (json_get_value(environ.start, environ.len, "broadcast", &value)) {
		netconf_json2inet((void *)dhcpc->nc.bc, &value);
	}

	if (json_get_value(environ.start, environ.len, "router", &value)) {
		netconf_json2inet((void *)dhcpc->nc.gw, &value);
	}

	if (json_get_value(environ.start, environ.len, "dns", &value)) {
		if ((value.type == JSON_TYPE_STRING) && (value.len >= 3)) {
			s = 1;
			c = 0;
			for (i = 2; i < value.len; i++) {
				if ((value.start[i] == ' ') || (i == (value.len - 1))) {
					if (c == 0) {
						netconf_str2inet((void *)dhcpc->nc.ns1, &value.start[s], i - s);
						s = i + 1;
						c = 1;
					} else if (c == 1) {
						netconf_str2inet((void *)dhcpc->nc.ns2, &value.start[s], i - s);
						break;
					}
				}
				if (i == (value.len - 1))
					break;
			}
		}
	}

	printf("DHCPC: IP: %s, NETMASK: %s, BROADCAST: %s, GATEWAY: %s, DNS1: %s, DNS2: %s\n", dhcpc->nc.ip, dhcpc->nc.nm, dhcpc->nc.bc, dhcpc->nc.gw, dhcpc->nc.ns1, dhcpc->nc.ns2);

/*	net_ifconfig(dhcpc->interface, dhcpc->ip, dhcpc->netmask, dhcpc->broadcast, 1);
	net_route_default(dhcpc->interface, dhcpc->gateway);
	// net_dns(idhcpc->interface, dns1, dns2);
	system("/bin/ntpclient -s -h time.google.com");
	dhcpc_status_set(dhcpc, DHCPC_STATUS_STARTED);
	if(dhcpc->callback != NULL)dhcpc->callback(1);

	dhcpc->lease = 3600 * 1000;*/
	dhcpc->refresh = 1;
	dhcpc->offer = 1;
	return 1;
}

