#ifndef _INTERFACE_H_
#define _INTERFACE_H_

#include "daemon.h"
#include "netconf.h"
#include "json.h"

struct interface_t {
	struct daemon_t *daemon;
	int status;
	struct netconf_t nc;
	json_object_t interface;
	json_object_t configuration;
};

struct interface_t *interface_new(json_object_t *interface, json_object_t *configuration);
void interface_start(struct interface_t *i);
void interface_stop(struct interface_t *i);

int interface_link_get(const char *iface);

void interface_lan_dynamic_proc(struct daemon_t *d, void *user);
void interface_lan_static_proc(struct daemon_t *d, void *user);

void interface_eg25_proc(struct daemon_t *d, void *user);

#endif

