#include "interface.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "daemon.h"
#include "dhcpc.h"

void interface_lan_dynamic_proc(struct daemon_t *d, void *user)
{
	struct interface_t *in = user;
	struct dhcpc_t *dhcpc;

	dhcpc = dhcpc_new();
	if (dhcpc == NULL)
		return;

	netconf_ifconfig("eth0", NULL, NULL, NULL, NULL, NULL, NULL, 0);

	while(1) {
		while (1) {
			if (interface_link_get("eth0"))
				break;

			if (daemon_test(d, 200))
				goto stop;
		}
		printf("STARTING DHCPC\n");
		dhcpc_start(dhcpc, "eth0");
		while (1) {
			if (!interface_link_get("eth0"))
				break;
			if (daemon_test(d, 200))
				goto stop;
			if (dhcpc_netconf(dhcpc, &in->nc)) {
				printf("INTERFACE: IP: %s, NETMASK: %s, BROADCAST: %s, GATEWAY: %s\n", in->nc.ip, in->nc.nm, in->nc.bc, in->nc.gw);
				netconf_ifconfig("eth0", in->nc.ip, in->nc.nm, in->nc.bc, in->nc.gw, in->nc.ns1, in->nc.ns2, 1);
				// netconf_route_default("eth0", in->nc.gw);
			}
		}
		printf("STOPPING DHCPC\n");
		dhcpc_stop(dhcpc);
		netconf_ifconfig("eth0", NULL, NULL, NULL, NULL, NULL, NULL, 0);
	}
stop:
	dhcpc_stop(dhcpc);
	dhcpc_free(dhcpc);
}

void interface_lan_static_proc(struct daemon_t *d, void *user)
{
	struct interface_t *in = user;
	json_object_t netconf, value;

	if (json_get_value(in->configuration.start, in->configuration.len, "netconf", &netconf)) {
		if (json_get_value(netconf.start, netconf.len, "address", &value))
			netconf_json2inet(in->nc.ip, &value);

		if (json_get_value(netconf.start, netconf.len, "netmask", &value))
			netconf_json2inet(in->nc.nm, &value);

		if (json_get_value(netconf.start, netconf.len, "broadcast", &value))
			netconf_json2inet(in->nc.bc, &value);

		if (json_get_value(netconf.start, netconf.len, "gateway", &value))
			netconf_json2inet(in->nc.gw, &value);

		if (json_get_value(netconf.start, netconf.len, "dns", &value))
			netconf_json2inet(in->nc.ns1, &value);

		if (json_get_value(netconf.start, netconf.len, "dns2", &value))
			netconf_json2inet(in->nc.ns2, &value);
	}
	netconf_ifconfig("eth0", in->nc.ip, in->nc.nm, in->nc.bc, in->nc.gw, in->nc.ns1, in->nc.ns2, 1);
	while (1) {
		if (daemon_test(d, 200))
			break;
	}
}

