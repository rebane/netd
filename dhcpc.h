#ifndef _DHCPC_H_
#define _DHCPC_H_

#include "daemon.h"
#include "netconf.h"

struct dhcpc_t{
	struct daemon_t *daemon;
	char interface[16];
	pthread_mutex_t lock;
	volatile int pid;
	volatile int refresh;
	volatile int offer;
	volatile unsigned int lease;
	volatile struct netconf_t nc;
};

struct dhcpc_t *dhcpc_new();
void dhcpc_free(struct dhcpc_t *dhcpc);
void dhcpc_start(struct dhcpc_t *dhcpc, char *interface);
void dhcpc_stop(struct dhcpc_t *dhcpc);
int dhcpc_netconf(struct dhcpc_t *dhcpc, struct netconf_t *nc);

#endif

