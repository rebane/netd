#ifndef _NET_H_
#define _NET_H_

#include "json.h"

#define NET_TYPE_ETHERNET    0
#define NET_TYPE_POINTOPOINT 1

void net_init();
int net_json2inet(char *dest, json_object_t *value);
int net_str2inet(char *dest, void *ptr, int len);
void net_up(char *interface, int type, char *ip, char *netmask, char *broadcast, char *pointopoint, char *gateway, char *dns1, char *dns2);
void net_down(char *interface);
void net_ifconfig(char *interface, char *ip, char *netmask, char *broadcast, int up);
void net_route_default(char *interface, char *gateway);
void net_dns(char *interface, char *dns1, char *dns2);

#endif

