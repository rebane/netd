#ifndef _DAEMON_H_
#define _DAEMON_H_

#include <pthread.h>

struct daemon_t {
	pthread_mutex_t lock;
	volatile int status;
	pthread_t thread;
	void *proc;
	void *user;
};

typedef void (*daemon_proc_t)(struct daemon_t *d, void *user);

struct daemon_t *daemon_new(daemon_proc_t proc, void *user);
void daemon_free(struct daemon_t *d);
void daemon_start(struct daemon_t *d);
void daemon_stop(struct daemon_t *d);
void daemon_restart(struct daemon_t *d);
int daemon_test(struct daemon_t *d, unsigned int timeout);
void daemon_exit(struct daemon_t *d);

#endif

