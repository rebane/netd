#include "interface.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include "json.h"
#include "mtimer.h"
#include "daemon.h"
#include "process.h"
#include "dhcpc.h"

void interface_eg25_proc(struct daemon_t *d, void *user)
{
	struct interface_t *in = user;
	struct dhcpc_t *dhcpc;
	char path[1024], buffer[1024], node[32], interface[16], apn[128];
	mtimer_t timer;
	json_object_t device, ping, ap, value;
	struct dirent *dent;
	DIR *dir;
	int len, ok, i;
	int pid, status;
	int ping_interval = 0, ping_error = 0, roaming = -1;
	FILE *f;

	dhcpc = dhcpc_new();
	if (dhcpc == NULL)
		return;

	if (!json_get_value(in->interface.start, in->interface.len, "device", &device))
		return;

	if (!json_get_value(device.start, device.len, "path", &value) || (value.type != JSON_TYPE_STRING))
		return;

	len = value.len - 2;
	if (len > 1020)
		len = 1020;

	memcpy(path, &value.start[1], len);
	path[len] = 0;

	strncpy(apn, "internet", 128);
	if (json_get_value(in->configuration.start, in->configuration.len, "apn", &ap)) {
		if (ap.type == JSON_TYPE_STRING) {
			len = ap.len - 2;
			if (len > 120)
				len = 120;

			memcpy(apn, &ap.start[1], len);
			apn[len] = 0;
		} else if (ap.type == JSON_TYPE_MAP) {
			if (json_value_isequal(ap.start, ap.len, "type", "string")) {
				if (json_get_value(ap.start, ap.len, "name", &value) && (value.type == JSON_TYPE_STRING)) {
					len = value.len - 2;
					if (len > 120)
						len = 120;

					memcpy(apn, &value.start[1], len);
					apn[len] = 0;
				}
			}else if (json_value_isequal(ap.start, ap.len, "type", "file")) {
				if (json_get_value(ap.start, ap.len, "file", &value) && (value.type == JSON_TYPE_STRING)) {
					len = value.len - 2;
					if (len > 1000)
						len = 1000;

					memcpy(buffer, &value.start[1], len);
					buffer[len] = 0;
					f = fopen(buffer, "r");
					if(f != NULL){
						memset(apn, 0, 128);
						fgets(apn, 120, f);
						fclose(f);
						apn[120] = 0;
						for(i = 127; i >= 0; i--){
							if((apn[i] != 0) && (apn[i] != '\n') && (apn[i] != '\r'))
								break;
							apn[i] = 0;
						}
						printf("APN: %s\n", apn);
					}
				}
			}
		}
	}
	if (json_get_value(in->configuration.start, in->configuration.len, "roaming", &value)) {
		if (value.type == JSON_TYPE_TRUE)
			roaming = 1;
		else
			roaming = 0;
	}

	if (json_get_value(in->configuration.start, in->configuration.len, "ping", &ping) && (ping.type == JSON_TYPE_MAP)) {
		if (json_get_value(ping.start, ping.len, "interval", &value))
			ping_interval = json_int(&value);

		if (json_get_value(ping.start, ping.len, "error", &value))
			ping_error = json_int(&value);
	}

	interface[0] = 0;

	printf("WAN EG25: APN: %s\n", apn);
	printf("WAN EG25: PING INTERVAL: %d, PING ERROR: %d\n", ping_interval, ping_error);

	while(1) {
		// POWER OFF
		if (json_get_value(device.start, device.len, "disable", &value) && (value.type == JSON_TYPE_STRING)) {
			len = value.len - 2;
			if (len > 1020)
				len = 1020;

			printf("INTERFACE EG25: POWER OFF\n");

			if (strlen(interface)) {
				netconf_ifconfig(interface, NULL, NULL, NULL, NULL, NULL, NULL, 0);
				sleep(2);
				interface[0] = 0;
			}

			memcpy(buffer, &value.start[1], len);
			buffer[len] = 0;
			system(buffer);
			sleep(2);
		}
		// POWER ON
		if (json_get_value(device.start, device.len, "enable", &value) && (value.type == JSON_TYPE_STRING)) {
			len = value.len - 2;
			if (len > 1020)
				len = 1020;

			memcpy(buffer, &value.start[1], len);
			buffer[len] = 0;

			printf("INTERFACE EG25: POWER ON\n");
			system(buffer);
			sleep(30);
		}

		// NODE
		snprintf(buffer, 1020, "%s/usbmisc", path);
		dir = opendir(buffer);
		if (dir == NULL)
			continue;

		node[0] = 0;
		while ((dent = readdir(dir)) != NULL) {
			if (dent->d_name[0] == 'c') {
				snprintf(node, 32, "/dev/%s", dent->d_name);
				break;
			}
		}
		closedir(dir);
		if (node[0] == 0)
			continue;

		// INTERFACE
		snprintf(buffer, 1020, "%s/net", path);
		dir = opendir(buffer);
		if (dir == NULL)
			continue;

		interface[0] = 0;
		while ((dent = readdir(dir)) != NULL) {
			if (dent->d_name[0] == 'w') {
				snprintf(interface, 16, "%s", dent->d_name);
				break;
			}
		}
		closedir(dir);
		if (interface[0] == 0)
			continue;

		// SETUP
		printf("WAN EG25: MODEM NODE: %s\n", node);
		printf("WAN EG25: MODEM INTERFACE: %s\n", interface);

		if (json_get_value(device.start, device.len, "raw_ip", &value) && (value.type == JSON_TYPE_TRUE)) {
			snprintf(buffer, 1020, "echo Y >/sys/class/net/%s/qmi/raw_ip", interface);
			printf("RAW IP\n");
		} else {
			snprintf(buffer, 1020, "echo N >/sys/class/net/%s/qmi/raw_ip", interface);
		}
		system(buffer);

		mtimer_timeout_set(&timer, 120000);
		while (!mtimer_timeout(&timer)) {
			if (roaming == 1) {
				pid = process("/bin/uqmi", "--device", node, "--keep-client-id", "wds", "--set-network-roaming", "any", "--start-network", apn);
			} else if (roaming == 0) {
				pid = process("/bin/uqmi", "--device", node, "--keep-client-id", "wds", "--set-network-roaming", "off", "--start-network", apn);
			} else {
				pid = process("/bin/uqmi", "--device", node, "--keep-client-id", "wds", "--start-network", apn);
			}
			while (!process_done(pid, &status)) {
				usleep(100000);
			}
			if(status == 0)
				break;
		}
		if (status != 0)
			continue;

		printf("STARTING DHCPC\n");
		mtimer_timeout_set(&timer, 30000);
		ok = 0;
		dhcpc_start(dhcpc, interface);

		while (!mtimer_timeout(&timer)) {
			if (daemon_test(d, 200))
				goto stop;

			if (dhcpc_netconf(dhcpc, &in->nc)) {
				printf("INTERFACE: IP: %s, NETMASK: %s, BROADCAST: %s, GATEWAY: %s\n", in->nc.ip, in->nc.nm, in->nc.bc, in->nc.gw);
				netconf_ifconfig(interface, in->nc.ip, in->nc.nm, in->nc.bc, in->nc.gw, in->nc.ns1, in->nc.ns2, 1);
				// netconf_route_default(interface, in->nc.gw);
				ok = 1;
				break;
			}
		}
		if (!ok) {
			dhcpc_stop(dhcpc);
			continue;
		}

		mtimer_timeout_set(&timer, ping_interval * 1000);
		ok = 0;
		while (1) {
			if (daemon_test(d, 200))
				goto stop;

			if (mtimer_timeout(&timer)) {
				if (ping_interval) {
					if (WEXITSTATUS(system("ping -c 1 1.1.1.1 -W 2 >/dev/null 2>&1")) == 0) {
						ok = 0;
						printf("PING OK\n");
					} else {
						ok++;
						printf("PING FAILED\n");
						if (ping_error) {
							if (ok >= ping_error) {
								printf("PING ERROR\n");
								break;
							}
						}
					}
				}
				mtimer_timeout_set(&timer, ping_interval * 1000);
			}
		}

		printf("STOPPING DHCPC\n");
		dhcpc_stop(dhcpc);
		netconf_ifconfig(interface, NULL, NULL, NULL, NULL, NULL, NULL, 0);
	}
stop:
	dhcpc_stop(dhcpc);
	dhcpc_free(dhcpc);
}

