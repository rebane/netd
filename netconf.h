#ifndef _NETCONF_H_
#define _NETCONF_H_

#include "json.h"

struct netconf_t {
	char ip[16];
	char nm[16];
	char bc[16];
	char gw[16];
	char ns1[16];
	char ns2[16];
};

int netconf_json2inet(char *dest, json_object_t *value);
int netconf_str2inet(char *dest, void *ptr, int len);

void netconf_init();
void netconf_ifconfig(char *interface, char *ip, char *netmask, char *broadcast, char *gateway, char *ns1, char *ns2, int up);
void netconf_route_default(char *interface, char *gateway);

#endif

